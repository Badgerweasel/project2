var express = require('express');
var router = express.Router();
var about_dal = require('../model/about_dal');

router.get('/', function (req, res) {
    res.render('about/front', req.query);
});

module.exports = router;