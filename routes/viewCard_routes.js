var express = require('express');
var router = express.Router();
var viewCard_dal = require('../model/viewCard_dal');


router.get('/faction', function (req, res) {
    viewCard_dal.faction(function (err, result) {
        if(err)
        {
            res.send(err);
        }
        else
        {
            res.render('viewCard/faction', {faction: result});
        }
    });
});

router.get('/allCards', function (req, res) {
    viewCard_dal.allCards(req.query.faction, function (err, result){
        viewCard_dal.countCards(req.query.faction, function (err, count) {
            if(err)
            {
                res.send(err);
            }
            else
            {
                res.render('viewCard/allCards', {card: result, model_count: count[0]});
            }
        });
    });
});

router.get('/card', function (req, res) {
    viewCard_dal.card(req.query.model_name, function (err, result) {
        if(err)
        {
            res.send(err);
        }
        else
        {
            console.log(JSON.stringify(result));
            res.render('viewCard/card', {stats:result[0][0], abilities:result[1]});
        }
    });
});

module.exports = router;