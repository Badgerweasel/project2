var express = require('express');
var router = express.Router();
var list_dal = require('../model/list_dal');

router.get('/all', function (req, res) {
    list_dal.viewAll(function(err, result){
        if(err)
        {
            res.send(err);
        }
        else
        {
            res.render('list/viewAll', {lists: result});
        }
    });
});

router.get('/getList', function(req, res) {
    list_dal.getList(req.query.list_name, function (err, result) {
        if(err)
        {
            res.send(err);
        }
        else
        {
            console.log(JSON.stringify(result));
            res.render('list/viewList', {models: result[0], list_name: req.query.list_name});
        }
    });
});

module.exports = router;