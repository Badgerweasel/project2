var express = require('express');
var router = express.Router();
var reviews_dal = require('../model/reviews_dal');

router.get('/all', function (req, res) {
    console.log('In reviews/all');
    reviews_dal.viewAll(function (err, result) {
        if(err)
        {
            res.send(err);
        }
        else
        {
            console.log(JSON.stringify(result));
            res.render('reviews/viewAll', {review: result, successful: req.query.successful});
        }
    });
});

router.get('/add', function(req, res){
    reviews_dal.email(function (err, result) {
        if(err)
        {
            res.send(err);
        }
        else
        {
            res.render('reviews/email', {email: result, successful:req.body.successful});
        }
    });
});

router.get('/factions', function(req, res){
    reviews_dal.factions(function (err, result){
        if(err)
        {
            res.send(err);
        }
        else
        {
            res.render('reviews/factions', {faction:result, email: req.query.email});
        }
    })
})

router.get('/allCards', function(req, res){
    reviews_dal.allCards(req.query.faction, function (err, result){
        if(err)
        {
            res.send(err);
        }
        else
        {
            res.render('reviews/allCards', {card: result, email: req.query.email});
        }
    });
});

router.get('/description', function(req, res){
    res.render('reviews/description', {model_name: req.query.model_name, email: req.query.email});
})

router.post('/insert', function(req, res){
    reviews_dal.insert(req.body, function(err, result){
        if(err)
        {
            res.send(err);
        }
        else
        {
            res.redirect('all/?successful=true');
        }
    });
});

module.exports = router;