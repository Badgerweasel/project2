var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.faction = function (callback) {
    var query = 'Select faction From models Group By faction;';

    connection.query(query, function(err, result) {
        console.log(JSON.stringify(result))
        callback(err, result);
    });
}

exports.allCards = function (faction, callback) {
    var query = 'Select model_name From models Where faction ="' + faction + '";';
    console.log(query);
    connection.query(query, function(err, result) {
        console.log(JSON.stringify(result))
        callback(err, result);
    });
}

exports.card = function (model_name, callback) {
    var query = 'Call view_card("' + model_name + '");';
    console.log(query);
    connection.query(query, function (err, result) {
        console.log('In card: ' + JSON.stringify(result));
        callback(err, result);
    });
}

exports.countCards = function(faction, callback) {
    var query = 'SELECT COUNT(model_name) AS model_count FROM models GROUP BY faction HAVING faction = "' + faction + '";';
    connection.query(query, function (err, result) {
        console.log('In card: ' + JSON.stringify(result));
        callback(err, result);
    });
}