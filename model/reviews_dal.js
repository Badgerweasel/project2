var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.viewAll = function(callback){
    var query = "Select * From review_faction;";

    connection.query(query, function(err, result) {
        console.log(JSON.stringify(result))
        callback(err, result);
    });
}

exports.email = function(callback){
    var query = 'Select email From player_account;'

    connection.query(query, function(err, result) {
        console.log(JSON.stringify(result))
        callback(err, result);
    });
}

exports.factions = function(callback){
    var query = 'Select faction From models Group By faction;';

    connection.query(query, function(err, result) {
        console.log(JSON.stringify(result))
        callback(err, result);
    });
}

exports.allCards = function (faction, callback) {
    var query = 'Select model_name From models Where faction ="' + faction + '";';
    console.log(query);
    connection.query(query, function(err, result) {
        console.log(JSON.stringify(result));
        callback(err, result);
    });
}

exports.insert = function (data, callback) {
    var query = 'Insert Into reviews( email, model_name, description) Values ("' + data.email + '", "' +
        data.model_name + '", "' + data.description + '");';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
}