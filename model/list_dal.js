var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.viewAll = function(callback){
    var query = 'Select list_name, email From army_list;';

    connection.query(query, function(err, result) {
        console.log(JSON.stringify(result))
        callback(err, result);
    });
}

exports.getList = function(list_name, callback) {
    var query = 'Call view_list("' + list_name + '");'

    connection.query(query, function(err, result) {
        console.log(JSON.stringify(result))
        callback(err, result);
    });
}